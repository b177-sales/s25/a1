// 2
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$name", total: {$count: {} }}}
]);

// 3
db.fruits.aggregate([
    {$match: {stock: {"$gt": 20}}},
    {$group: {_id: "$name", total: {$count: {} }}}
]);

// 4
db.fruits.aggregate([
     { $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" }}}
]);

// 5
db.fruits.aggregate([
    {$group: { _id: "$supplier_id", total: {$max: "$price"}}}
]);

// 6
db.fruits.aggregate([
    {$group: { _id: "$supplier_id", total: {$min: "$price"}}}
]);